<?php
	function DB_Connect(){ //function parameters, two variables.
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "avensisdb";
		
		$conn = mysqli_connect($servername, $username, $password, $dbname);
		
		// Check connection
		if (!$conn) {
			die("Connection failed: " . mysqli_connect_error());
		}		
		
		//echo "Done connection";
		return $conn;  //returns the second argument passed into the function
  }		
  function DB_Close($conn){
		mysqli_close($conn);
		// echo "Connection close";
  }
  
?>