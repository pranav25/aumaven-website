-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 01, 2018 at 11:00 AM
-- Server version: 5.6.24
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `aumaven`
--

-- --------------------------------------------------------

--
-- Table structure for table `beaninstructor`
--

CREATE TABLE IF NOT EXISTS `beaninstructor` (
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `subjectofint` varchar(100) NOT NULL,
  `indexp` int(5) NOT NULL,
  `aboutyou` varchar(300) NOT NULL,
  `message` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `beaninstructor`
--

INSERT INTO `beaninstructor` (`name`, `email`, `contact`, `subjectofint`, `indexp`, `aboutyou`, `message`) VALUES
('aS', 'nirali.thakkar2013@gmail.com', 'AS', 'as', 5, 'ASas', 'AS'),
('Gaurav Mohan Baru', 'gauravthakkar1991@gmail.com', '', 'Gaurav Mohan Baru', 5, 'FDGDF', 'DFGD'),
('Gaurav Mohan Baru', 'gauravthakkar1991@gmail.com', 'fdsf', 'Gaurav Mohan Baru', 5, 'GBDCF', 'SDFSD'),
('tesj', 'sadsd@da', '', 'asfasf', 32, 'safsa', 'fsf'),
('wefe1', 'dsfds@rfwa', '', 'dsada', 31, 'sadsad', 'fssf'),
('dsad', 'sdsa@sfaf', '', 'dasd', 23, 'dwad', 'sfa'),
('wdw', 'dsad@fsa', '13123', 'dsad', 12, 'dsadas', 'dsadas'),
('dsds', 'adaddasdas@adasd', '132112342142', 'dawdadad', 13, 'dasdasfsaf', 'sadfudshfuidshfds dsafhas');

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE IF NOT EXISTS `careers` (
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `areaofexpertise` varchar(100) NOT NULL,
  `indexp` int(5) NOT NULL,
  `aboutcandidate` varchar(300) NOT NULL,
  `uploadcvlocation` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `careers`
--

INSERT INTO `careers` (`name`, `email`, `contact`, `areaofexpertise`, `indexp`, `aboutcandidate`, `uploadcvlocation`) VALUES
('aS', 'nirali.thakkar2013@gmail.com', 'AS', 'as', 5, 'ASas', 'AS'),
('Gaurav Mohan Baru', 'gauravthakkar1991@gmail.com', '', 'Gaurav Mohan Baru', 5, 'FDGDF', 'DFGD'),
('Gaurav Mohan Baru', 'gauravthakkar1991@gmail.com', 'fdsf', 'Gaurav Mohan Baru', 5, 'GBDCF', 'SDFSD'),
('dwqdwq`qwewqdq', 'asdada@dasd', '131241241', 'dd', 54, 'sadasd', '../uploads/animate.css'),
('asdsad', 'regre@etgr', '1231343', 'asd', 33, 'sdsa', '../uploads/animate.css');

-- --------------------------------------------------------

--
-- Table structure for table `hireus`
--

CREATE TABLE IF NOT EXISTS `hireus` (
  `hireusid` int(10) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `others` varchar(300) NOT NULL,
  `contactno` varchar(15) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hireus`
--

INSERT INTO `hireus` (`hireusid`, `firstname`, `email`, `subject`, `others`, `contactno`) VALUES
(1, 'Krishna Kutir', 'gaurav.thakkar1991@gmail.com', 'sdsad', 'xcvxvxc', ''),
(2, 'asd', 'asd', 'asd', 'asd', ''),
(3, 'asd', 'as', 'asd', 'as', ''),
(4, 'Krishna Kutir', 'nirali.thakkar2013@gmail.com', 'asdasd', 'sdf', ''),
(5, 'Krishna Kutir', 'nirali.thakkar2013@gmail.com', 'asd', 'asd', ''),
(6, 'asd', 'asds@gmail.com', 'asd', 'asd', ''),
(7, 'Krishna Kutir', 'nirali.thakkar2013@gmail.com', 'asdasd', 'asS', ''),
(8, 'sdfsdf', 'nirali.thakkar2013@gmail.com', 'sdf', 'sdf', ''),
(9, 'Krishna Kutir', 'nirali.thakkar2013@gmail.com', 'sdc', 'sdc', ''),
(10, 'Krishna Kutir', 'nirali.thakkar2013@gmail.com', '', 'AS', ''),
(11, 'Gaurav Baru', 'gaurav.baru@qualitykiosk.com', '', 'asda', ''),
(12, 'Gaurav Baru', 'gaurav.baru@qualitykiosk.com', '', 'asdasd', ''),
(13, 'Gaurav Mohan Baru', 'gauravthakkar1991@gmail.com', '', 'zxczxc', ''),
(14, 'Gaurav Baru', 'gaurav.baru@qualitykiosk.com', '', 'cbdcb', ''),
(15, 'priyanka', 'gaurav.thakkar1991@gmail.com', '', 'priyanka', ''),
(16, 'Gaurav Mohan Baru', 'gauravthakkar1991@gmail.com', '', 'asda', ''),
(17, 'wdaw', 'dsa@sfsd', 'sdas', 'dsad', ''),
(18, 'dd', 'sds@faf', '3123', 'e2e', '113123'),
(19, 'test', 'test@test', 'ada', 'da', '2132132142');

-- --------------------------------------------------------

--
-- Table structure for table `partnerconnect`
--

CREATE TABLE IF NOT EXISTS `partnerconnect` (
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `contact` varchar(10) NOT NULL,
  `client_type` varchar(50) NOT NULL,
  `message` varchar(300) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partnerconnect`
--

INSERT INTO `partnerconnect` (`name`, `email`, `contact`, `client_type`, `message`) VALUES
('asdaSD', 'asda', 'fdsf', 'sadsa', 'asd'),
('Gaurav Mohan Baru', 'gauravthakkar1991@gmail.com', '', 'individual', 'AS'),
('Gaurav Baru', 'gaurav.baru@qualitykiosk.com', 'fdsf', 'business', 'asdas'),
('Gaurav Mohan Baru', 'gauravthakkar1991@gmail.com', 'fdsf', '', 'xcvxcv'),
('Gaurav Mohan Baru', 'gauravthakkar1991@gmail.com', 'fdsf', '', 'ZXZX'),
('dsff', 'wdwd@wdaa', '1231', 'individual', 'asdas'),
('dada`qweq', 'dsas2afsef@adas', '32133233', 'individual', 'adada');

-- --------------------------------------------------------

--
-- Table structure for table `subscribe`
--

CREATE TABLE IF NOT EXISTS `subscribe` (
  `subscribeemail` varchar(100) NOT NULL,
  `subscribenum` int(10) unsigned NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribe`
--

INSERT INTO `subscribe` (`subscribeemail`, `subscribenum`) VALUES
('gauravthakkar1991@gmail.com', 1),
('gauravthakkar1991@gmail.com', 2),
('asds@gmail.com', 3),
('fireball_fire87@yahoo.com', 4),
('gauravthakkar1991@gmail.com', 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hireus`
--
ALTER TABLE `hireus`
  ADD PRIMARY KEY (`hireusid`);

--
-- Indexes for table `subscribe`
--
ALTER TABLE `subscribe`
  ADD KEY `subscribenum` (`subscribenum`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hireus`
--
ALTER TABLE `hireus`
  MODIFY `hireusid` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `subscribe`
--
ALTER TABLE `subscribe`
  MODIFY `subscribenum` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
